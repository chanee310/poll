function loadPollData() {
    axios.get('/poll').then(function(resp) {
		var data = resp.data;
		// 제목
		document.querySelector('#poll-subject').innerText = data.title;
		// 링크
		document.querySelector('#poll-link').innerText = data.link;
		document.querySelector('#poll-link').href = data.link;
		// 내용 지움
		document.querySelector('.graph').innerHTML = '';

		document.querySelector('#poll-result').innerText = '전체 ' + data.answerCount + '명 투표, 총합 ' + data.totalCount + '표'
		var list = '';

		var max = 0;
		for (var i = 0; i < data.result.length; i++) {
			var per = Math.ceil((data.result[i].count / data.totalCount) * 100);
			list += '<li><div class="flex-wrap"><p class="graph-item">' + data.result[i].name + ' (득표 수 : ' + data.result[i].count + '/' + data.totalCount + ')</p><p class="graph-per">' + per + '%</p></div><p class="percentage" style="width: ' + per + '%"></p></li>';
		}

		document.querySelector('.graph').innerHTML = list;
	});
}

window.onload = function() {
    loadPollData();
}
