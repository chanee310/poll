var express = require('express');
var router = express.Router();

const dcinside = require('../../nodeinside');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index');
});

router.get('/poll', function(req, res, next) {
    const gall_id = 'chungha';
    const article_no = 184742;
    const maxPick = 5;
    const pickType = 1;

    dcinside.comment.fetch(gall_id, article_no).then(data => {
        const regexAnswer = /\d+\.\d+\.\d+\.\d+\.\d+/;
        const regexAnswer2 = /\d+\s\d+\s\d+\s\d+\s\d+/;
        let poll = {
            title: '생일써폿 메인 5Pick 투표',
            list: [
                '',
                '코스튬 뱃지',
                '맥북',
                '와인',
                '퍼즐액자',
                '트레이닝복',
                '피규어',
                '블루레이 플레이어',
                '무드등',
                '홍삼',
                '입욕제',
                '힛더스트로피제작',
                '초콜릿',
                '상품권',
                '극세사담요',
                '술잔제작',
                '향수',
                '레코더',
                '발마사지기',
                '블루투스 스피커',
                '녹음용 마이크',
                '네일 건조기',
                '목마사지기',
                '셔츠',
                '지하철광고',
                '맨투맨',
                '사쿠라술',
                '사진액자',
                '스프링베어에코백',
                '립스틱',
                '탄생석목걸이',
                '후드',
                '클러치',
                '기부or후원',
                '자수정귀걸이',
                '바디필로우',
                '마약쿠션(밤비용)',
                '런닝화',
                '핸드크림',
                '키보드',
                '가디건',
                '선글라스',
                '쟈켓',
                '쿠션',
                '안마시트',
                '스커트',
                '스니커즈',
                '시계',
                '샴페인',
                '토드백',
                '진(술)',
                '디퓨저',
                '모자',
                'lay',
                '페이셜오일',
                '캡슐커피머신+캡슐',
                '커스텀 인이어',
                '미니 스쿠터'
            ],
            result : [],
            answerCount : 0,
            totalCount : 0,
            link : `http://gall.dcinside.com/board/view/?id=${gall_id}&no=${article_no}`
        };

        // 결과 초기화
        for (let idx in poll.list) {
            poll.result[idx] = {
                name : poll.list[idx],
                count : 0
            }
        }

        for (let idx = 0; idx < data.comment_list.length; idx += 1) {
            let comment = data.comment_list[idx];
            comment.comment_memo = comment.comment_memo.replace(/\D+/g, '.').replace(/\.{2,}/g, '.');
            if (regexAnswer.test(comment.comment_memo)) {
                let answer = comment.comment_memo.split('.');
                for (let i = 0; i < maxPick; i += 1) {
                    if (pickType === 0) {
                        // 그냥 1점씩
                        poll.result[answer[i]].count += 1;
                        poll.totalCount += 1;
                    } else if (pickType === 1) {
                        // 5점부터 순차적용
                        poll.result[answer[i]].count += (maxPick - i);
                        poll.totalCount += (maxPick - i);
                    }
                }
                poll.answerCount += 1;
            }
        }

        // 정렬
        poll.result.sort(function(a, b) {
            return a.count < b.count ? 1 : a.count > b.count ? -1 : 0;
        });

        res.json(poll);
    });
});

module.exports = router;
